import React from 'react';
import { 
    BrowserRouter as Router,
    Route,
    Redirect,
} from 'react-router-dom';
import Navigation from '../Navigation';
import SignInPage from '../Login';
import PasswordForgetPage from '../PasswordForget';
import Home from '../Home';
import GiftsCompletePage from '../GiftsComplete'

import * as ROUTES from '../../constants/routes';
import { withAuthentication } from '../Session';

const App = () => (
    <Router>
        <div>
            <Route path={ROUTES.SIGN_IN} component={SignInPage} />
            <Route path={ROUTES.FORGOT_PASSWORD} component={PasswordForgetPage} />
            <Route path={ROUTES.HOME} component={Home} />
            <Route path={ROUTES.GIFTS_COMPLETE} component={GiftsCompletePage} />
            <Redirect to={ROUTES.SIGN_IN} />
        </div>
    </Router> 
)

export default withAuthentication(App);