import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withAuthorization } from '../Session';

import {
  Row,
  Col
} from 'reactstrap';

import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';
import Navigation from '../Navigation';

const limitRows = 5; //Always the same

const Home = () => (
  <div className="content">
    <Navigation size="sm" />
    <Row style={{ margin: 20 }}>
      <Gifts title={'Gifts Ordered'} status="Ordered" limit={limitRows} />
      <Gifts
        title={'Gifts delivered to address'}
        status="Delivered"
        limit={limitRows}
      />
      <Gifts
        title={'Gifts not delivered to address'}
        status="SelectedDelivery"
        limit={limitRows}
      />
      <Gifts
        title={'Gifts already picked up in store'}
        status="PickedUp"
        limit={limitRows}
      />
      <Gifts
        title={'Gifts not picked up in store'}
        status="SelectedPickUp"
        limit={limitRows}
      />
    </Row>
  </div>
);

const condition = authUser => !!authUser;

class GiftsBase extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      giftsOrdered: []
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    // Get last limit gifts with status
    this.props.firebase
      .gifts()
      .where('status', '==', this.props.status)
      .orderBy('date_bought', 'desc')
      .limit(this.props.limit)
      .onSnapshot(snapshot => {
        if (snapshot.size) {
          let gifts = [];
          snapshot.forEach(doc => gifts.push({ ...doc.data(), uid: doc.id }));

          this.setState({
            giftsOrdered: gifts,
            loading: false
          });
        } else {
          this.setState({ giftsOrdered: null, loading: false });
        }
      });
  }

  render() {
    const { title, status } = this.props;
    const { giftsOrdered, loading } = this.state;
    return (
      <div style={{width:'100%'}}>
        {loading ? (
          <div>Loading ...</div>
        ) : (
          <GiftList gifts={giftsOrdered} title={title} status={status} />
        )}
      </div>
    );
  }
}

const Gifts = withFirebase(GiftsBase);

const GiftList = ({ gifts, title, status }) => (
  <Col xs={12}>
    <h4 style={{marginBottom:20}}>{title}</h4>
    <div style={{ overflowX:'auto' }}>
      <table style={{width:'max-content', minWidth:'100%' }}>
        <thead className="text-primary">
          <tr>
            <th className="text-center">Product</th>
            <th className="text-center">Brand</th>
            <th className="text-right">Price</th>
            <th className="text-center">City</th>
            <th className="text-center">Country</th>
            <th className="text-center">Gifter</th>
            <th className="text-center">Gift Receiver</th>
            {status.indexOf('Deliver') >= 0 && (
              <th className="text-center">Shipping address</th>
            )}
            {status.indexOf('Pick') >= 0 && (
              <th className="text-center">Store address</th>
            )}
            <th className="text-center">Gift Bought</th>
            <th className="text-center">Gift Opened</th>
            {status.indexOf('Delivered') >= 0 && (
              <th className="text-center">Gift Delivered</th>
            )}
            <th className="text-center">Status</th>
          </tr>
        </thead>
        <tbody>
          {gifts != null
            ? gifts.map(gift => (
                <GiftItem key={gift.uid} gift={gift} status={status} />
              ))
            : ''}
        </tbody>
      </table>
    </div>
    <GiftFooter status={status} />
  </Col>
);

const GiftItem = ({ gift, status }) => {
  let year;
  let month;
  let day;
  let dateBought;
  let dateOpened;
  let dateSent;
  if (gift.date_bought) {
    year = gift.date_bought.toDate().getFullYear();
    month = gift.date_bought.toDate().getMonth() + 1;
    day = gift.date_bought.toDate().getDate();
    dateBought =
      year +
      '-' +
      month.toString().padStart(2, '0') +
      '-' +
      day.toString().padStart(2, '0');
  }

  if (gift.date_opened && gift.date_opened !== '') {
    year = gift.date_opened.toDate().getFullYear();
    month = gift.date_opened.toDate().getMonth() + 1;
    day = gift.date_opened.toDate().getDate();
    dateOpened =
      year +
      '-' +
      month.toString().padStart(2, '0') +
      '-' +
      day.toString().padStart(2, '0');
  }

  if (gift.date_sent && gift.date_sent !== '') {
    year = gift.date_sent.toDate().getFullYear();
    month = gift.date_sent.toDate().getMonth() + 1;
    day = gift.date_sent.toDate().getDate();
    dateSent =
      year +
      '-' +
      month.toString().padStart(2, '0') +
      '-' +
      day.toString().padStart(2, '0');
  }

  const price = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  }).format(gift.gift_info.price);

  return (
    <tr>
      <td className="text-center">{gift.gift_info.product}</td>
      <td className="text-center">{gift.gift_info.brand}</td>
      <td className="text-right">{price}</td>
      <td className="text-center">{gift.gift_info.city}</td>
      <td className="text-center">{gift.country}</td>
      <td className="text-center">{gift.gift_giver.giver_email}</td>
      <td className="text-center">{gift.gift_receiver.receiver_email}</td>
      {status.indexOf('Deliver') >= 0 && (
        <td className="text-left">Av. ABC 123 ...</td>
      )}
      {status.indexOf('Pick') >= 0 && (
        <td className="text-left">Av. XYZ 789 ...</td>
      )}
      <td className="text-center">{dateBought}</td>
      <td className="text-center">{dateOpened}</td>
      {status.indexOf('Delivered') >= 0 && (
        <td className="text-center">{dateSent}</td>
      )}
      <td className="text-center">{gift.status}</td>
    </tr>
  );
};

const GiftFooter = ({ status }) => {
  return (
      <Link to={`${ROUTES.GIFTS}/${status}`} className="primary" style={{color:"#FFFFFF"}}>View all</Link>
    
  );
};
export default withAuthorization(condition)(Home);
