import React from "react";
import { Link } from 'react-router-dom';

import SignOutButton from './SignOut';
import * as ROUTES from '../constants/routes';
import { AuthUserContext } from './Session';

class PanelHeader extends React.Component {
    render() {
        return (
            <div
                className={
                    "panel-header " +
                    (this.props.size !== undefined
                        ? "panel-header-" + this.props.size
                        : "")
                }
            >
                <AuthUserContext.Consumer>
                    {authUser =>
                    authUser ? <NavigationAuth /> : <NavigationNonAuth />
                    }
                </AuthUserContext.Consumer>
            </div>
        );
    }
}

const NavigationAuth = () => (
    <ul> 
      <li>
        <Link to={ROUTES.LANDING}>Landing</Link>
      </li> 
      <li>
        <Link to={ROUTES.HOME}>Home</Link>
      </li>
      <li>
        <SignOutButton />
      </li>
    </ul>
  );
  
  const NavigationNonAuth = () => (
    <ul>
      <li>
        <Link to={ROUTES.LANDING}>Landing</Link>
      </li>
      <li>
        <Link to={ROUTES.SIGN_IN}>Sign In</Link>
      </li>
    </ul> 
  );
  
export default PanelHeader;
