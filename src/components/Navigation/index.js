import React from 'react';
import { Link } from 'react-router-dom';

import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import { AuthUserContext } from '../Session';
import Logo from '../../assets/img/logo.png';

class Navigation extends React.Component {
  render() {
    return (
      <AuthUserContext.Consumer>
        {authUser => (authUser ? <NavigationAuth /> : <NavigationNonAuth />)}
      </AuthUserContext.Consumer>
    );
  }
}

const NavigationAuth = () => (
  <header>
    <div className="container">
      <div className="row">
        <div className="col-4">
          <Link to={ROUTES.HOME}>
            <img src={Logo} alt="logo" style={{ width: 146 }} />
          </Link>
        </div>
        <div className="col-8">
        <div style={{float:'right'}}>
          <SignOutButton  />
        </div>
        </div>
      </div>
    </div>
  </header>
);

const NavigationNonAuth = () => <ul />;

export default Navigation;
