//export const LANDING = '/';
export const SIGN_IN = '/signin';
export const HOME = '/home';
export const FORGOT_PASSWORD = '/forgot-password';
export const GIFTS = '/gifts'
export const GIFTS_COMPLETE = '/gifts/:status';